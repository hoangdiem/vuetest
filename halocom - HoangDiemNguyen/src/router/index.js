import Vue from 'vue'
import VueRouter from 'vue-router'
import Post from '@/components/Post';
import PostDetail from '@/components/PostDetail';
Vue.use(VueRouter)
export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Post',
      component: Post
    },
    {
      path: '/post/:id',
      name: 'Post Detail',
      props: true,
      component: PostDetail
    },
  ],
  mode: 'history'
})